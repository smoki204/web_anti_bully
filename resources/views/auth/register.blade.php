@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Register') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('register') }}">
                            @csrf

                            <div class="content-header">
                                <div class="container-fluid">
                                    <div class="row mb-2">
                                        <div class="col-sm-6">
                                            <h1 class="m-0">Halaman data siswa</h1>
                                        </div><!-- /.col -->
                                        <div class="col-sm-6">
                                            <ol class="breadcrumb float-sm-right">
                                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                                <li class="breadcrumb-item active">Data Siswa</li>
                                            </ol>
                                        </div><!-- /.col -->
                                    </div><!-- /.row -->
                                </div><!-- /.container-fluid -->
                            </div>
                            <!-- /.content-header -->

                            <!-- Main content -->
                            <div class="content">
                                <div class="card card-info card-outline">
                                    <div class="card-header">
                                        <div class="card-tools">
                                            <button onclick="window.print()">Print</button>
                                            <a href="{{ route('create-siswa') }}" class="btn btn-success">Tambah Data <i
                                                    class="fas fa-plus-square"></i></a>
                                        </div>
                                    </div>




                                    <div class="card-body">
                                        <table class="table table-bordered" id="user">
                                            <tr>
                                                <th>id</th>
                                                <th>Username</th>
                                                <th>Password</th>
                                                <th>Phone</th>
                                                <th>Gender</th>
                                                <th>email</th>
                                                <th>level</th>
                                                <th>action</th>
                                            </tr>
                                            @foreach ($user as $i => $sis)
                                                <tr>
                                                    <td>{{ $sis->id }}</td>
                                                    <td>{{ $sis->username }}</td>
                                                    <td>{{ $sis->password }}</td>
                                                    <td>{{ $sis->phone }}</td>
                                                    <td>{{ $sis->gender }}</td>
                                                    <td>{{ $sis->email }}</td>
                                                    <td>{{ $sis->level }}</td>
                                                    <td>
                                                        <a href="{{ route('edit-siswa', $sis->id) }}"
                                                            class="btn btn-secondary">Edit</a>
                                                        @method('delete')
                                                        <a href="{{ url('delete-siswa', $sis->id) }}"
                                                            class="btn btn-danger">Delete</a>
                        </form>
                        </td>
                        </tr>
                        @endforeach
                        </table>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    </div>
@endsection
