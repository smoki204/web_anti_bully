
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    @include('template.head')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
@include('template.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Halaman data siswa</h1>
          </div><!-- /.col -->

        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
      <div class="content">
        <div class="card card-info card-outline">
          <div class="card-header">
            <h3>Edit Siswa</h3>
          </div>
          <div class="card-body">
            <form action="{{ url('update-siswa', $data->id)}}" method="post">
              @csrf
              <div class="form-group">
                <input type="text" id="username" name="username" class="form-control" placeholder="Username" value="{{ $data->username}}"> 
                </div>
              <div class="form-group">
                <input type="text" id="password" name="password" class="form-control" placeholder="password" value="{{ $data->password}}"> 
                </div>
                <div class="form-group">
                  <input type="text" id="phone" name="phone" class="form-control" placeholder="Phone" value="{{ $data->phone}}"> 
                  </div>
                  <div class="form-group">
                    <select class="form-control select2" style="width: 100%;" name="gender">
                       <option value="{{ $data->gender}}"> Pilih Gender</option>
                       @foreach ($user as $item)
                       <option value="{{ $item->id }}">{{ $item->gender}}</option>
                       @endforeach
                    </select>
                </div>
                <div class="form-group">
                  <input type="text" id="email" name="email" class="form-control" placeholder="Email" value="{{ $data->email}}"> 
                  </div>
                  <div class="form-group">
                    <select class="form-control select2" style="width: 100%;" name="level" >
                       <option value="{{ $data->level}}">Level</option>
                       @foreach ($user as $item)
                       <option value="{{ $item->id }}">{{ $item->level}}</option>
                       @endforeach
                    </select>
                </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-success">Ubah data</button>
                  </div>
              </form> 
            </div>
        </div>
      </div>
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
@include('template.footer')
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{asset('AdminLTE/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('AdminLTE/dist/js/adminlte.min.js"></script')}}">
</body>
</html>
