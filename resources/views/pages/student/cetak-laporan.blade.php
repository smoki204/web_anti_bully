<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        table.stati {}

    </style>
    <title>CETAK PENGADUAN</title>
</head>
<body>
    <div class="form-group">
        <p align="center"><b>Laporan Data Pegawai</b></p>
        <table class="static" align="center" rules="all" border="1px" style="width: 95%;"></table>
        <tr>
            <th>id</th>
            <th>school_class</th>
            <th>report_title</th>
            <th>report_detail</th>
            <th>incident_time</th>
            <th>place</th>
            <th>type_of_bullying</th>
            <th>victim_name</th>
            <th>class</th>
            <th>reporter_name</th>
            <th>proof</th>
            <th>photo_desription</th>
            <th>responses</th>
            <th>verification</th>
        </tr>
        <tr>
            @foreach ($cetakcomplaint as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->school_class }}</td>
                <td>{{ $item->report_title }}</td>
                <td>{{ $item->report_detail }}</td>
                <td>{{ date('d-m-y', strtotime($item->incident_time)) }}</td>
                <td>{{ $item->place }}</td>
                <td>{{ $item->type_of_bullying }}</td>
                <td>{{ $item->victim_name }}</td>
                <td>{{ $item->class }}</td>
                <td>{{ $item->reporter_name }}</td>
                <td>{{ $item->proof }}</td>
                <td>{{ $item->photo_desription }}</td>
                <td>{{ $item->responses }}</td>
                <td>{{ $item->verification }}</td>
                <td>
            @endforeach
        </tr>
    </div>
</body>
</html>