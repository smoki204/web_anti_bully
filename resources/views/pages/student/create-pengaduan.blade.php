
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    @include('template.head')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
@include('template.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
@include('template.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Stop Bullying!</h1>
          </div><!-- /.col -->

        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
      <div class="content col-xl-6">
        <div class="card card-info card-outline">
          <div class="card-header">
            <h3>Lapor pengaduan mu!</h3>
          </div>
          <div class="card-body ">
            <form action="{{ route('simpan-laporan')}}" method="post" enctype="multipart/form-data">
              {{ csrf_field()}}
              <div class="form-group">
                <input type="text" id="report_title" name="report_title" class="form-control" placeholder="report_title"> 
                </div>
                <div class="form-group">
                  <textarea name="report_detail" id="report_detail"  class="form-control" placeholder="report_detail"></textarea>
                  </div>
                <div class="form-group">
                  <input type="date" id="incident_time" name="incident_time" class="form-control" placeholder="incident_time"> 
                  </div>
                <div class="form-group">
                  <input type="text" id="place" name="place" class="form-control" placeholder="place"> 
                  </div>
                <div class="form-group">
                  <input type="text" id="type_of_bullying" name="type_of_bullying" class="form-control" placeholder="type_of_bullying"> 
                  </div>
                <div class="form-group">
                  <input type="text" id="victim_name" name="victim_name" class="form-control" placeholder="victim_name"> 
                  </div>
                <div class="form-group">
                    <input type="text" id="class" name="class" class="form-control" placeholder="class"> 
                  </div>
                <div class="form-group">
                  <input type="text" id="reporter_name" name="reporter_name" class="form-control" placeholder="{{ Auth::user()->username }}" disabled>
                  </div>
                <div class="form-group">
                    <label for="">Proof</label>
                  <input type="file" id="proof" name="proof"> 
                  </div>
                <div class="form-group">
                  <textarea name="photo_desription" id="photo_desription"  class="form-control" placeholder="photo_desription"></textarea>
                  </div>
                
                  
                  <div class="form-group">
                    <button type="submit" class="btn btn-success">Simpan</button>
                  </div>
              </form>
            </div>
        </div>
      </div>
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
@include('template.footer')
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{asset('AdminLTE/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('AdminLTE/dist/js/adminlte.min.js"></script')}}">
</body>
</html>
