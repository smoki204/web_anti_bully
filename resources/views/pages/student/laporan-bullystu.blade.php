<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<link rel="stylesheet" href="/css/style.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

<head>
    @include('template.head')

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
</script>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top bg-dark">
    <div class="container-fluid">
        <h1>Bully | Blocker</h1>
        <ul class="navbar-nav"> 
            <li class="nav-item">
                <a class="nav-link" href="{{ route('halaman-home') }}" style="margin-right: 30px">Report</a>
            </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('laporan-bullystu') }}" style="margin-right: 30px">Report History</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('/logout') }}" style="margin-right: 10px;">Logout</a>
        </li>
      </ul>
    </div>
</nav>

<style>
    .use {
        margin-left: 950px;
    }

    h1 {
        font-size: 50px !important;
        background: linear-gradient(90deg, #ff0000, #ffff00, #ff00f3, #0033ff, #ff00c4, #ff0000);
        background-size: 400%;
        font-size: 40px;
        letter-spacing: 5px;
        font-weight: 600;
        word-spacing: 5px;
        -webkit-text-fill-color: transparent;
        -webkit-background-clip: text;
        animation: animate 10s linear infinite;
    }

    @keyframes animate {
        0% {
            background-position: 0%;
        }

        100% {
            background-position: 400%;
        }
    }
</style>
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Navbar -->

        <!-- /.navbar -->

        <!-- Main Sidebar Container -->

        <!-- Content Wrapper. Contains page content -->
        <div class="wrapper">

            <h2 class="at d-print-none" data-text="Report History">Report History</h2>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
               
                        <table class="table table-bordered">
                            <tr>
                                <th>id</th>
                                <th>Victim's Name</th>
                                <th>Class</th>
                                <th>Time Happend</th>
                                <th>Place</th>
                                <th>Type of Bullying</th>
                                <th>Reporter Id</th>
                                <th>Proof</th>
                                <th>Photo Description</th>
                                <th>Responses</th>
                                <th class="d-print-none">Status</th>
                            </tr>
                            @foreach ($complaint as $i => $sis)
                                <tr>
                                    <td>{{ $sis->id }}</td>
                                    <td>{{ $sis->victim_name }}</td>
                                    <td>{{ $sis->class }}</td>
                                    <td>{{ date('d-m-y', strtotime($sis->incident_time)) }}</td>
                                    <td>{{ $sis->place }}</td>
                                    <td>{{ $sis->type_of_bullying }}</td>
                                    <td>{{ $sis->reporter_id }}</td>
                                    <td style="width: 300px;">
                                        <img src="{{ asset('/proof/' . $sis->proof) }}" height="150px" width="250px"
                                            alt="#" srcset="">
                                    </td>
                                    <td>{{ $sis->photo_desription }}</td>
                                    <td> <a href="{{ route('edit-siswa', $sis->id) }}"
                                            class="btn btn-secondary">{{ $sis->responses }}</a></td>
                                    <td>
                                        <a href="{{ url('', $sis->id) }}"
                                            class="btn btn-primary d-print-none">{{ $sis->verification }}</a>
                                    </td>
                                    </form>

                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        @include('template.footer')
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    @include('sweetalert::alert')

    <style>
        table{
            color: white;
            margin-top: 50px;
        }

        body {
            background-color: #000;
        }

        .at{
            margin-left: 800px;
        }

        th{
            color: white;
        }

        td{
            color: white;
        }
    </style>
</body>

</html>
