
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    @include('stunav.head')

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
</script>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top bg-dark">
    <div class="container-fluid">
        <h1>Bully | Blocker</h1>
        <ul class="navbar-nav"> 
            <li class="nav-item">
                <a class="nav-link" href="{{ route('halaman-home') }}" style="margin-right: 30px">Report</a>
            </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('laporan-bullystu') }}" style="margin-right: 30px">Respond</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('/logout') }}" style="margin-right: 10px;">Logout</a>
        </li>
      </ul>
    </div>
</nav>

<style>
    .use {
        margin-left: 950px;
    }

    h1 {
        font-size: 50px !important;
        background: linear-gradient(90deg, #ff0000, #ffff00, #ff00f3, #0033ff, #ff00c4, #ff0000);
        background-size: 400%;
        font-size: 40px;
        letter-spacing: 5px;
        font-weight: 600;
        word-spacing: 5px;
        -webkit-text-fill-color: transparent;
        -webkit-background-clip: text;
        animation: animate 10s linear infinite;
    }

    @keyframes animate {
        0% {
            background-position: 0%;
        }

        100% {
            background-position: 400%;
        }
    }
</style>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
@include('stunav.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="width: 1800px">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Laporan bully</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Generate Lporan</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="card card-info card-outline">
            <div class="card-header">
                <div class="card-tools">
                  <button onclick="window.print()">Print</button>
                    <a href="{{route('create-pengaduan')}}" class="btn btn-success">lapor<i class="fas fa-plus-square"></i></a>
                </div>
            </div>

    <div class="card-body" style="width: 1000px">
          <table class="table table-bordered">
                  <tr>
                      <th>id</th>
                      <th>Report Title</th>
                      <th>report_detail</th>
                      <th>Incident Time</th>
                      <th>Place</th>
                      <th>Type of Bullying</th>
                      <th>Victim's Name</th>
                      <th>Class</th>
                      <th>Reporter Id</th>
                      <th>Proof</th>
                      <th>Photo Description</th>
                      <th>Responses</th>
                      <th class="d-print-none">verification</th>
                  </tr>
                  @foreach ($complaint as $i => $sis)
                      <tr>
                          <td>{{ $sis->id }}</td>
                          <td>{{ $sis->report_title }}</td>
                          <td>{{ $sis->report_detail }}</td>
                          <td>{{ date('d-m-y', strtotime($sis->incident_time)) }}</td>
                          <td>{{ $sis->place }}</td>
                          <td>{{ $sis->type_of_bullying }}</td>
                          <td>{{ $sis->victim_name }}</td>
                          <td>{{ $sis->class }}</td>
                          <td>{{ $sis->reporter_id }}</td>
                          <td>{{ $sis->proof }}</td>
                          <td>{{ $sis->photo_desription }}</td>
                          <td> <a href="{{ route('edit-siswa', $sis->id)}}" class="btn btn-secondary">{{ $sis->responses }}</a></td>
                          <td>
                            <a href="{{ url('', $sis->id)}}" class="btn btn-primary d-print-none">{{ $sis->verification }}</a>
                          </td>
                            </form>
                         
                      </tr>
                  @endforeach
          </table>
      </div>
  </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h1>Ini halaman data user</h1>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
@include('stunav.footer')
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
@include('sweetalert::alert')

<!-- jQuery -->
<script src="{{asset('AdminLTE/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('AdminLTE/dist/js/adminlte.min.js"></script')}}">
</body>
</html>
