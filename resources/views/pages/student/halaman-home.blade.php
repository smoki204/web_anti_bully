<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bully | Blocker</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="sweetalert2.all.min.js"></script>
        <link rel="stylesheet" href="sweetalert2.min.css">


    <nav class="navbar  navbar-dark shadow-5-strong">
        <div class="nav" style="margin-top: 30px;">
            <h1>Report Form</h1>
            <a href="{{ url('/student-home') }}" class="btn btn-primary" style="height: 40px; margin-left: 600px;">Home</a>
        </div>
        @if (Route::has('login'))
            <div class="sm:fixed sm:top-0 sm:right-0 p-6 text-right">
                @auth
                @else
                    <a href="{{ route('login') }}"
                        class="font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Log
                        in</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}"
                            class="ml-4 font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Register</a>
                    @endif
                @endauth
            </div>
        @endif
    </nav>

    @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <p class="text-danger">{{ $error }}</p>
                @endforeach
            @endif


</head>

<body>
    <div class="forum">
        <div class="card">
            <h2>Report Data</h2>
            <form action="{{ route('laporan-student') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="type_of_bullying" style="width: 400px">Victim's Name</label>
                    <input type="text" id="victim_name" name="victim_name" class="form-control">
                </div>
                <div class="form-group">
                    <label for="" style="width: 400px">Education Degree's</label>
                    <select id="class" name="class" class="form-control">
                        <option value=""></option>
                        <option value="TK">TK</option>
                        <option value="SD">SD</option>
                        <option value="SMP">SMP</option>
                        <option value="SMK">SMK</option>
                        <option value="unknown">unknown</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="type_of_bullying" style="width: 400px">Date of Occurrence</label>
                    <input type="date" id="incident_time" name="incident_time" class="form-control">
                </div>
                <div class="form-group">
                    <label for="type_of_bullying" style="width: 400px">Place Occurred</label>
                    <input type="text" id="place" name="place" class="form-control">
                </div>
                <div class="form-group">
                    <label for="type_of_bullying" style="width: 400px">Type of Bullying</label>
                    <select name="type_of_bullying" class="form-control" id="type_of_bullying">
                        <option value=""></option>
                        <option value="Verbal">Verbal</option>
                        <option value="Fisik">Physic</option>
                        <option value="Sosial">Social</option>
                        <option value="Cyberbullying">Cyberbullying</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Reporter's ID</label>
                    <input type="text" id="reporter_id" name="reporter_id" class="form-control"
                        value="{{ auth()->user()->id }}" readonly>
                </div>
                <div class="form-group">
                    <label for="">Proof</label>
                    <input type="file" id="proof" name="proof" accept="image/png,image/jpeg">
                </div>
                <div class="form-group">
                    <label for="type_of_bullying" style="width: 400px">Photo Description</label>
                    <textarea name="photo_desription" id="photo_desription" class="form-control" style="height: 100px;"></textarea>
                </div>


                <div class="form-group">
                    <button type="submit" class="btn btn-success">Send</button>
                </div>
            </form>
        </div>
        <div class="image">
            <img src="/images/antibully.png" alt="" srcset="">
        </div>
    </div>
    <style>
        .forum{
            margin-bottom: 50px;
        }
        .alert{
            width: 100px;
            height: 50px;
        }
        body {
            background-color: #0c192c;
        }

        h1 {
            margin-left: 600px;
            color: #4fc3dc;
            box-shadow: 0 0 0 10px #4fc3dc44,
                0 0 50px #4fc3dc;
            margin-bottom: 100px;
        }

        label {
            margin-left: 10px;
            color: #ffffff;
            margin-bottom: 5px;
        }

        h2 {
            text-align: center;
            margin-bottom: 30px;
            color: #4fc3dc;
        }

        .form-group {
            margin-bottom: 20px;
            width: 92%;
            margin-left: 20px;
        }

        .forum {
            display: flex;
            margin-left: 100px;
        }

        .card {
            width: 50%;
            margin-left: 100px;
            background-color: #214d8e;
            box-shadow: 0 0 0 10px #4fc3dc44,
                0 0 50px #4fc3dc;
            
        }

        img {
            width: 65%;
            margin-left: 50px;
            border-radius: 10px;
            height: 850px;
            box-shadow: 0 0 0 10px #4fc3dc44,
                0 0 50px #4fc3dc;
        }
    </style>
 @include('sweetalert::alert')
</body>

</html>
