<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    @include('template.head')
    @include('template.navbar')
</head>

<body>
    <div class="bg-home">
        <h2 class="at" data-text="stop bullying">stop bullying</h2>
        <h2 class="at" data-text="bullying can destroy someone's life" style="margin-left: 570px;">bullying can destroy someone's life</h2>
    </div>
    <div class="poto">
      <img src="/images/car1.jpg">
      <img src="/images/car2.jpg">
    </div>





    <style>

      img{
        margin-top: 100px;
        width: 600px;
        margin-left: 110px;
        border-radius: 20px;
      }
        body {
            height: 100%;
            width: 100%;
            overflow: hidden;
            background-color: #000;
        }

        .at {
            margin-left: 820px;
            color: #fff;
            font-weight: 300;
            font-size: 30px;
            text-transform: uppercase;
            transform: scale(2);
            letter-spacing: 2px;
            padding: 30px;
        }

        .at:before,
        .at:after {
            position: absolute;
            content: attr(data-text);
            padding: 30px;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            background: #000;
            color: #fff;
            overflow: hidden;
        }

        .at:before {
            left: 3px;
            text-shadow: -2px 0 red;
            animation: glitch-1 2s 0s linear reverse infinite;
        }

        .at:after {
            left: -3px;
            text-shadow: -2px 0 blue;
            animation: glitch-2 2s 0s linear reverse infinite;
        }

        @keyframes glitch-1 {
            0% {
                clip: rect(132px, auto, 101px, 30px);
            }

            5% {
                clip: rect(17px, auto, 94px, 30px);
            }

            10% {
                clip: rect(40px, auto, 66px, 30px);
            }

            15% {
                clip: rect(87px, auto, 82px, 30px);
            }

            20% {
                clip: rect(137px, auto, 61px, 30px);
            }

            25% {
                clip: rect(34px, auto, 14px, 30px);
            }

            30% {
                clip: rect(133px, auto, 74px, 30px);
            }

            35% {
                clip: rect(76px, auto, 107px, 30px);
            }

            40% {
                clip: rect(59px, auto, 130px, 30px);
            }

            45% {
                clip: rect(29px, auto, 84px, 30px);
            }

            50% {
                clip: rect(22px, auto, 67px, 30px);
            }

            55% {
                clip: rect(67px, auto, 62px, 30px);
            }

            60% {
                clip: rect(10px, auto, 105px, 30px);
            }

            65% {
                clip: rect(78px, auto, 115px, 30px);
            }

            70% {
                clip: rect(105px, auto, 13px, 30px);
            }

            75% {
                clip: rect(15px, auto, 75px, 30px);
            }

            80% {
                clip: rect(66px, auto, 39px, 30px);
            }

            85% {
                clip: rect(133px, auto, 73px, 30px);
            }

            90% {
                clip: rect(36px, auto, 128px, 30px);
            }

            95% {
                clip: rect(68px, auto, 103px, 30px);
            }

            100% {
                clip: rect(14px, auto, 100px, 30px);
            }
        }

        @keyframes glitch-2 {
            0% {
                clip: rect(132px, auto, 101px, 30px);
            }

            5% {
                clip: rect(17px, auto, 94px, 30px);
            }

            10% {
                clip: rect(40px, auto, 66px, 30px);
            }

            15% {
                clip: rect(87px, auto, 82px, 30px);
            }

            20% {
                clip: rect(137px, auto, 61px, 30px);
            }

            25% {
                clip: rect(34px, auto, 14px, 30px);
            }

            30% {
                clip: rect(133px, auto, 74px, 30px);
            }

            35% {
                clip: rect(76px, auto, 107px, 30px);
            }

            40% {
                clip: rect(59px, auto, 130px, 30px);
            }

            45% {
                clip: rect(29px, auto, 84px, 30px);
            }

            50% {
                clip: rect(22px, auto, 67px, 30px);
            }

            55% {
                clip: rect(67px, auto, 62px, 30px);
            }

            60% {
                clip: rect(10px, auto, 105px, 30px);
            }

            65% {
                clip: rect(78px, auto, 115px, 30px);
            }

            70% {
                clip: rect(105px, auto, 13px, 30px);
            }

            75% {
                clip: rect(15px, auto, 75px, 30px);
            }

            80% {
                clip: rect(66px, auto, 39px, 30px);
            }

            85% {
                clip: rect(133px, auto, 73px, 30px);
            }

            90% {
                clip: rect(36px, auto, 128px, 30px);
            }

            95% {
                clip: rect(68px, auto, 103px, 30px);
            }

            100% {
                clip: rect(14px, auto, 100px, 30px);
            }
        }
    </style>
</body>

</html>
