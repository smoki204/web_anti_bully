<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<link rel="stylesheet" href="/css/style.css">

<head>
    @include('template.head')


</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Navbar -->
        @include('template.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <h2 class="at" data-text="Respond">Respond</h2>

        <!-- Main content -->
        <div class="content">
            <div class="card card-info card-outline">
                <div class="card-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>id</th>
                            <th>Victim's Name</th>
                            <th>Class</th>
                            <th>Time Happend</th>
                            <th>Place</th>
                            <th>Type of Bullying</th>
                            <th>Reporter Id</th>
                            <th style="width: 300px;">Proof</th>
                            <th>Photo Description</th>
                            <th>Responses</th>
                            <th>Status</th>
                        </tr>
                        @foreach ($complaint as $i => $sis)
                            <tr>
                                <td>{{ $sis->id }}</td>
                                <td>{{ $sis->victim_name }}</td>
                                <td>{{ $sis->class }}</td>
                                <td>{{ date('d-m-y', strtotime($sis->incident_time)) }}</td>
                                <td>{{ $sis->place }}</td>
                                <td>{{ $sis->type_of_bullying }}</td>
                                <td>{{ $sis->reporter_id }}</td>
                                <td style="width: 300px;">
                                    <img src="{{ asset('/proof/' . $sis->proof) }}" height="150px" width="250px"
                                        alt="#" srcset="">
                                </td>
                                <td>{{ $sis->photo_desription }}</td>
                                <td>
                                    <form method="POST" action="{{ route('perundungan.update', ['id' => $sis->id]) }}">
                                        @csrf
                                        @method('PUT')
                                        <select name="responses">
                                            <option value="Confirmed"
                                                {{ $sis->responses == 'Confirmed' ? 'selected' : '' }}>Confirmed
                                            </option>
                                            <option value="Hoax" {{ $sis->responses == 'Hoax' ? 'selected' : '' }}>
                                                Hoax
                                            </option>
                                            <option value="Unknown"
                                                {{ $sis->responses == 'Unknown' ? 'selected' : '' }}>
                                                Unknown
                                            </option>
                                        </select>
                                        <button class="btn btn-primary" type="submit">Update</button>
                                    </form>
                                </td>
                                <td>
                                    <form method="POST"
                                        action="{{ route('perundungan.update', ['id' => $sis->id]) }}">
                                        @csrf
                                        @method('PUT')
                                        <select name="verification">
                                            <option value="waiting"
                                                {{ $sis->verification == 'Waiting' ? 'selected' : '' }}>Waiting
                                            </option>
                                            <option value="process"
                                                {{ $sis->verification == 'Process' ? 'selected' : '' }}>Process
                                            </option>
                                            <option value="finished"
                                                {{ $sis->verification == 'Finished' ? 'selected' : '' }}>Finished
                                            </option>
                                        </select>
                                        <button class="btn btn-primary" type="submit">Update</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.content -->

    <!-- /.content-wrapper -->

    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    @include('template.footer')
    <!-- ./wrapper -->


    <!-- REQUIRED SCRIPTS -->
    @include('sweetalert::alert')

    <style>
        body{
            background-color: black;    
        }

        .at{
            margin-left: 900px;
        }
    </style>

</body>

</html>
