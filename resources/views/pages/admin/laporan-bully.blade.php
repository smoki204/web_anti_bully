<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<link rel="stylesheet" href="/css/style.css">

<head>
    @include('template.head')
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Navbar -->
        @include('template.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->

        <!-- Content Wrapper. Contains page content -->
        <div class="wrapper">

            <h2 class="at d-print-none" data-text="Generate Report">Generate Report</h2>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="card card-info card-outline">
                    <div class="card-header">
                        <button class="btn btn-primary d-print-none" style="margin-left: 20px;" onclick="window.print()">Print</button>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <tr>
                                <th>id</th>
                                <th>Victim's Name</th>
                                <th>Class</th>
                                <th>Time Happend</th>
                                <th>Place</th>
                                <th>Type of Bullying</th>
                                <th>Reporter Id</th>
                                <th>Proof</th>
                                <th>Photo Description</th>
                                <th>Responses</th>
                                <th class="d-print-none">Status</th>
                            </tr>
                            @foreach ($complaint as $i => $sis)
                                <tr>
                                    <td>{{ $sis->id }}</td>
                                    <td>{{ $sis->victim_name }}</td>
                                    <td>{{ $sis->class }}</td>
                                    <td>{{ date('d-m-y', strtotime($sis->incident_time)) }}</td>
                                    <td>{{ $sis->place }}</td>
                                    <td>{{ $sis->type_of_bullying }}</td>
                                    <td>{{ $sis->reporter_id }}</td>
                                    <td>
                                        <img src="{{ asset('/proof/' . $sis->proof) }}" height="200px" width="250px"
                                            alt="#" srcset="">
                                    </td>
                                    <td>{{ $sis->photo_desription }}</td>
                                    <td> <a href="{{ route('edit-siswa', $sis->id) }}"
                                            class="btn btn-secondary">{{ $sis->responses }}</a></td>
                                    <td>
                                        <a href="{{ url('', $sis->id) }}"
                                            class="btn btn-primary d-print-none">{{ $sis->verification }}</a>
                                    </td>
                                    </form>

                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        @include('template.footer')
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    @include('sweetalert::alert')

    <style>
        body {
            background-color: #000;
        }

        .at{
            margin-left: 800px
        }
    </style>
</body>

</html>
