<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
</script>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top bg-dark">
    <div class="container-fluid">
        <h1>Bully | Blocker</h1>
        <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="{{ route('data-siswaop') }}" style="margin-right: 10px">User Data</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('tanggapan-petugas') }}" style="margin-right: 10px">Respond</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('laporan-bully') }}" style="margin-right: 30px">Generate</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('/logout') }}" style="margin-right: 10px;">Logout</a>
        </li>
      </ul>
    </div>
</nav>

<style>
    .use {
        margin-left: 950px;
    }

    h1 {
        font-size: 50px !important;
        background: linear-gradient(90deg, #ff0000, #ffff00, #ff00f3, #0033ff, #ff00c4, #ff0000);
        background-size: 400%;
        font-size: 40px;
        letter-spacing: 5px;
        font-weight: 600;
        word-spacing: 5px;
        -webkit-text-fill-color: transparent;
        -webkit-background-clip: text;
        animation: animate 10s linear infinite;
    }

    @keyframes animate {
        0% {
            background-position: 0%;
        }

        100% {
            background-position: 400%;
        }
    }
</style>
