<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    use HasFactory;

    protected $table = 'complaints';

    protected $primaryKey = 'id';

    protected $fillable = [
        'victim_name',
        'class',
        'incident_time',
        'place',
        'type_of_bullying',
        'reporter_id',
        'proof',
        'photo_desription',
        'responses',
        'verification',
        'created_at',
        'updated_at',
    ];
}

