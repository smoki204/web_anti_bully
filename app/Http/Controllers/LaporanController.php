<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Complaint;
use Illuminate\Support\Facades\Auth;

class LaporanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->level != 'Student') {
            $complaint = Complaint::all();
        } else {
            $complaint = $user->complaints;
        }
        return view('pages.admin.laporan-bully', compact('complaint'));
    }

    public function cetaklaporan()
    {
        $cetakcomplaint = Complaint::all();
        return view('pages.student.cetak-laporan');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('pages.student.create-pengaduan');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        $request->validate([
            'victim_name' => 'required',
            'class' => 'required',
            'incident_time' => 'required',
            'place' => 'required',
            'type_of_bullying' => 'required',
            'reporter_id' => 'required',
            'proof' => 'required',
            'photo_desription' => 'required',
        ]);
        $complaint = Complaint::create([
            'victim_name' => $request->victim_name,
            'class' => $request->class,
            'incident_time' => $request->incident_time,
            'place' => $request->place,
            'type_of_bullying' => $request->type_of_bullying,
            'reporter_id' => $request->reporter_id,
            'proof' => $request->proof,
            'photo_desription' => $request->photo_desription,
        ]);

        return redirect()->back()->with('toast_success', 'Kamu berhasil Melapor');

        $path = $request->image->store('public/proof');
        $request['proof'] = str_replace('public/', '', $path);

        if($request->hasFile('proof')) {
            $file = $request->file('proof');
            $fileName = $file->getClientOriginalName();
            $destinationPath = public_path('/proof');
            $file->move($destinationPath, $fileName);
            $complaint->proof = $fileName;
            $complaint->save();
        }

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = Complaint::findOrFail($id);
        $data->delete();
        return back()->with('info', 'Data berhasil dihapus');
    }
}
