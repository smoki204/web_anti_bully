<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Complaint;

class LastudController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $complaint = Complaint::all();
        return view('pages.student.laporan-bullystu', compact('complaint'));
    }

    public function cetaklaporan()
    {
        $cetakcomplaint = Complaint::all();
        return view('pages.student.cetak-laporan');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('pages.student.create-pengaduan');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        $request->validate([
            'victim_name' => 'required',
            'class' => 'required',
            'incident_time' => 'required',
            'place' => 'required',
            'type_of_bullying' => 'required',
            'reporter_id' => 'required',
            'proof' => 'required',
            'photo_desription' => 'required',
        ]);
        $complaint = Complaint::create([
            'victim_name' => $request->victim_name,
            'class' => $request->class,
            'incident_time' => $request->incident_time,
            'place' => $request->place,
            'type_of_bullying' => $request->type_of_bullying,
            'reporter_id' => $request->reporter_id,
            'proof' => $request->proof,
            'photo_desription' => $request->photo_desription,
        ]);



        if($request->hasFile('proof')) {
            $file = $request->file('proof');
            $fileName = $file->getClientOriginalName();
            $destinationPath = public_path('/proof');
            $file->move($destinationPath, $fileName);
            $complaint->proof = $fileName;
            $complaint->save();
        }


        return redirect()->back()->with('toast_success', 'Report Sent');

        

        
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $totalReports = Complaint::count('id');

     return view('total_reports', ['totalReports' => $totalReports]);

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = Complaint::findOrFail($id);
        $data->delete();
        return back()->with('info', 'Data berhasil dihapus');
    }
}
