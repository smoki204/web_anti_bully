<?php

namespace Database\Seeders;

use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Hash;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        \App\Models\User::create([
            'name' => 'Admin',
            'password' => Hash::make('padhe'),
            'phone' => '089670029340',
            'gender' => 'Male',
            'email' => 'padhe@gmail.com',
            'level' => 'Administrator',
        ]);

        \App\Models\User::create([
            'name' => 'Operator',
            'password' => Hash::make('operator'),
            'phone' => '0895703330',
            'gender' => 'male',
            'email' => 'operator@gmail.com',
            'level' => 'Officers',
        ]);

        \App\Models\User::create([
            'name' => 'Student',
            'password' => Hash::make('gracia'),
            'phone' => '0896546221',
            'gender' => 'Female',
            'email' => 'student@gmail.com',
            'level' => 'Student',
        ]);
    }
}
