<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('complaints', function (Blueprint $table) {
            $table->id();
            $table->string('victim_name');
            $table->enum('class', ['TK', 'SD', 'SMP', 'SMK', 'unknown'])->default('unknown');
            $table->dateTime('incident_time');
            $table->string('place', 32);
            $table->string('type_of_bullying', 32);
            $table->foreignId('reporter_id')->constrained('users', 'id');
            $table->string('proof', 255);
            $table->text('photo_desription');
            $table->enum('responses', ['Confirmed', 'Hoax', 'Unknown'])->default('Unknown');
            $table->enum('verification', ['Waiting', 'Process', 'Finished'])->default('Waiting');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('complaints');
    }
};
